import random

# Um vetor auxiliar para indicar as movimentações (cima, direita, baixo, esquerda)
offset = [[-1,0], [0,1], [1,0], [0, -1]]

#Classe para representar cada nó(posicao do tabuleiro, com linha, coluna e a leitura dos sensores)
class Noh:
    def __init__(self, linha, coluna, sensores, vizinhos, hashcode):
        self.linha = linha
        self.coluna = coluna
        self.sensores = sensores
        self.vizinhos = vizinhos
        self.hashcode = hashcode

    #Função que retorna o nó como uma string
    def __str__(self):
        return f'Linha {self.linha}, Coluna{self.coluna}'

    #Função auxiliar para pegar a leitura dos sensores de cada nó
    def get_sensores(lab_str, linha, coluna):
        sensores = []
        for i in range(4):
            l=linha+offset[i][0]
            c=coluna+offset[i][1]
            if l>=0 and l<len(lab_str) and c>=0 and c<len(lab_str[0]) and lab_str[l][c]!='X':
                sensores.append(True)
            else:
                sensores.append(False)
        return sensores

    #Função auxiliar para pegar os vizinhos de cada n´o
    def get_vizinhos(labirinto, linha, coluna):
        vizinhos=[]
        for i in range(4):
            l=linha+offset[i][0]
            c=coluna+offset[i][1]
            if l>=0 and l<len(labirinto) and c>=0 and c<len(labirinto[0]):
                vizinhos.append(labirinto[l][c])
            else:
                vizinhos.append(None)
        return vizinhos

 

    #Função para gerar todos os nós no labirinto
    def gera_todos(lab_str):
        labirinto = [ [ Noh(i, j, None, None, i*len(lab_str)+j) for j in range(len(lab_str[i]))] for i in range(len(lab_str)) ]
        for linha in labirinto:
            for celula in linha:
                celula.sensores = Noh.get_sensores(lab_str, celula.linha, celula.coluna)
                celula.vizinhos = Noh.get_vizinhos(labirinto, celula.linha, celula.coluna)
        return [noh for linha in labirinto for noh in linha]

#Classe para representar um estado de crença para o labirinto, o movimento que levou até ele e suas funções
class Estado:
    def __init__(self, estado_de_crenca, acao, pai):
        self.estados = estado_de_crenca
        self.acao = acao
        self.pai = pai

    #Atualiza o estado de crença de acordo com os sensores
    def atualiza(self, sensores):
        self.estados = list(filter(lambda e: e.sensores==sensores, self.estados))

    #Gera os próximos estados vizinhos de acordo com o estado de crença
    def estados_vizinhos(self):
        estados_vizinhos = []
        for i in range(4):
            if (i+2)%4 == self.acao: #Não deixa voltar para o último visitado
                estados_vizinhos.append(None)
                continue
            if not self.estados[0].sensores[i]: #Não vai para posição com obstáculo
                estados_vizinhos.append(None)
                continue
            atual = Estado([],i, self)
            for e in self.estados:
                atual.estados.append(e.vizinhos[i])
            estados_vizinhos.append(atual)
        return estados_vizinhos

#Função subida de encosta para exemplificar a solução
def subida_enconta(estado_inicial, agente, max_iteracoes):
    estado_atual = estado_inicial
    estado_atual.atualiza(agente.sensores)
    vizinhos = estado_atual.estados_vizinhos()
    iteracao= 0
    while len(estado_atual.estados)!=1 and vizinhos.count(None)!=4 and iteracao<max_iteracoes:
        iteracao += 1
        tamanhos = list(map(lambda v: len(v.estados) if v != None else 100000000, vizinhos))
        melhor = tamanhos.index(min(tamanhos))
        estado_atual = vizinhos[melhor]
        agente = agente.vizinhos[melhor]
        estado_atual.atualiza(agente.sensores)
        vizinhos = estado_atual.estados_vizinhos()
    return estado_atual

#Função para exibir a previsão
def exibe_solucoes(estado_final):
    atual = estado_final
    solucoes = estado_final.estados
    while atual.pai!=None:
        for i in range(len(solucoes)):
            solucoes[i] = solucoes[i].vizinhos[(atual.acao+2)%4]
        atual = atual.pai
    for i in range(len(solucoes)):
        print("Solução: ", solucoes[i])


l = [[ '_','_','_','_','_'], ['_','X','_','_','_'], ['_','X','_','_','X'], ['_','_','X','_','_'], ['_','X','_','_','_']]
estado_inicial = Estado(Noh.gera_todos(l), None, None)
noh_agente = None
while noh_agente==None:
    noh_agente = random.choice(estado_inicial.estados)
print("Posicao real:",noh_agente)
estado_final = subida_enconta(estado_inicial, noh_agente, 50)
exibe_solucoes(estado_final)

