#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <ctime>

using namespace std;

// Um vetor auxiliar para indicar as movimentações (cima, direita, baixo, esquerda)
const int offset[][2] = {{-1,0}, {0,1}, {1,0}, {0, -1}};
// Número de linhas e colunas do labirinto
int LINHAS, COLUNAS=0;

 // Classe para representar cada nó(posicao do tabuleiro, com linha, coluna e a leitura dos sensores)
class Noh{

    public:
        // Atributos
        int linha, coluna;
        vector<bool> sensores;
        vector<Noh *> vizinhos;
        int hash;

        // Construtor
        Noh(int linha, int coluna, vector<bool> sensores, vector<Noh *> vizinhos, int hashcode){
            this->linha = linha;
            this->coluna = coluna;
            this->sensores = sensores;
            this->vizinhos = vizinhos;
            this->hash = hash;
        }

        string to_str(){
            return "Linha " + to_string(this->linha) + ", Coluna " + to_string(this->coluna);
        }

};


// Função auxiliar para pegar a leitura dos sensores de cada nó
template <size_t linhas, size_t colunas>
vector<bool> get_sensores(char (&lab_str)[linhas][colunas], int linha, int coluna){
    vector<bool> sensores;
    for(int i=0;i<4;i++){
        int l=linha+offset[i][0];
        int c=coluna+offset[i][1];
        if(l>=0 && l<LINHAS && c>=0 && c<COLUNAS && lab_str[l][c]!='X')
            sensores.push_back(true);
        else
            sensores.push_back(false);
    }
    return sensores;
}


// Função auxiliar para pegar os vizinhos de cada n´o
vector<Noh *> get_vizinhos (vector<vector<Noh *>>labirinto, int linha, int coluna){
    vector<Noh *> vizinhos;
    for(int i=0;i<4;i++){
        int l=linha+offset[i][0];
        int c=coluna+offset[i][1];
        if(l>=0 && l<LINHAS && c>=0 && c<COLUNAS)
            vizinhos.push_back(labirinto[l][c]);
        else
            vizinhos.push_back(nullptr);
    }
    return vizinhos;
}

 
// Função para gerar todos os nós no labirinto
template <size_t linhas, size_t colunas>
vector<Noh *> gera_todos(char (&lab_str)[linhas][colunas]){
    vector<vector<Noh *>> labirinto;
    for(int i=0;i<LINHAS;i++){
        vector<Noh *> linha;
        for(int j=0;j<COLUNAS;j++){
            Noh *noh = new Noh(i, j, vector<bool>(), vector<Noh *>(), i*LINHAS+j);
            linha.push_back(noh);
        }
        labirinto.push_back(linha);
    }

    for(int i=0;i<LINHAS;i++)
        for(int j=0;j<COLUNAS;j++){
            labirinto[i][j]->sensores = get_sensores(lab_str, i, j);
            labirinto[i][j]->vizinhos = get_vizinhos(labirinto, i, j);
        }
    vector<Noh *> todos;
    for(int i=0; i<LINHAS;i++)
        todos.insert(todos.end(), labirinto[i].begin(), labirinto[i].end()); 
    return todos;
}


// Classe para representar um estado de crença para o labirinto, o movimento que levou até ele e suas funções
class Estado{

    public:
        //Atributos
        vector<Noh *> estados;
        int acao;
        Estado *pai;

        Estado(vector<Noh *> estado_de_crenca, int acao, Estado *pai){
            this->estados = estado_de_crenca;
            this->acao = acao;
            this->pai = pai;
        }

        void atualiza(vector<bool> sensores){
            vector<Noh *> copia = estados;
            estados.clear();
            for(auto i = copia.begin(); i!=copia.end(); i++){
                Noh *atual = *i;
                if(atual->sensores == sensores)
                    this->estados.push_back(atual);
            }
        }

        //Gera os próximos estados vizinhos de acordo com o estado de crença
        vector<Estado *> gera_estados_vizinhos(){
            vector<Estado *> estados_vizinhos;
            for(int i=0;i<4;i++){
                if((i+2)%4 == this->acao){ //Não deixa voltar para o último visitado
                    estados_vizinhos.push_back(nullptr);
                    continue;
                }
                if(!this->estados[0]->sensores[i]){ // Não vai para posição com obstáculo
                    estados_vizinhos.push_back(nullptr);
                    continue;
                }
                Estado *atual = new Estado(vector<Noh *> (), i, this);
                for(auto e = this->estados.begin(); e!= this->estados.end();e++)
                    atual->estados.push_back((*e)->vizinhos[i]);
                estados_vizinhos.push_back(atual);
            }
            return estados_vizinhos;
        }
};


// Função subida de encosta para exemplificar a solução

Estado *subida_enconta(Estado *estado_inicial, Noh *agente, int max_iteracoes){
    Estado *estado_atual = estado_inicial;
    estado_atual->atualiza(agente->sensores);
    vector<Estado *> vizinhos = estado_atual->gera_estados_vizinhos();
    int iteracao= 0;
    while(estado_atual->estados.size()!=1 && count(vizinhos.begin(), vizinhos.end(), nullptr)!=4 && iteracao<max_iteracoes){
        iteracao++;
        vector<int> tamanhos;
        for(auto v=vizinhos.begin(); v!=vizinhos.end(); v++)
            tamanhos.push_back((*v)!=nullptr?(*v)->estados.size():100000000);
        int melhor = min_element(tamanhos.begin(),tamanhos.end()) - tamanhos.begin();
        estado_atual = vizinhos[melhor];
        agente = agente->vizinhos[melhor];
        estado_atual->atualiza(agente->sensores);
        vizinhos = estado_atual->gera_estados_vizinhos();
    }
    return estado_atual;
}


// Função para exibir a previsão
void exibe_solucoes(Estado *estado_final){
    Estado * atual = estado_final;
    vector<Noh *> solucoes = estado_final->estados;
    while(atual->pai!=nullptr){
        for(size_t i=0;i<solucoes.size();i++)
            solucoes[i] = solucoes[i]->vizinhos[(atual->acao+2)%4];
        atual = atual->pai;
    }
    for(size_t i=0;i<solucoes.size();i++)
         cout << "Solução: " << solucoes[i]->to_str() << endl;
}

int main(){
    srand(time(nullptr));
    char l[5][5]={{ '_','_','_','_','_'}, {'_','X','_','_','_'}, {'_','X','_','_','X'}, {'_','_','X','_','_'}, {'_','X','_','_','_'}};
    LINHAS=5;
    COLUNAS=5;
    Estado *estado_inicial = new Estado(gera_todos(l), -1, nullptr);
    Noh *noh_agente = nullptr;
    while(noh_agente==nullptr)
        noh_agente = estado_inicial->estados[rand()%estado_inicial->estados.size()];
    cout << "Posicao real:" << noh_agente->to_str() << endl;
    Estado *estado_final = subida_enconta(estado_inicial, noh_agente, 50);
    exibe_solucoes(estado_final);
    return 0;
}
