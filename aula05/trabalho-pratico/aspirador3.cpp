#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

/*
Para facilitar vamos definir a configuração do ambiente de 0 a 7, como em uma codificação binária para 
sujeira. Se for 0 é limpa, então 000 são todos as salas limpas, ja 111 são todas as salas sujas. Isso nos
dá 8 cenários. Nesse caso podemos convencionar
0 a 7 quando o robô está na 1ª sala (sala 0)
8 a 15 quando o robô está na 2ª sala (sala 1)
16 a 23 quando o robô está na 3ª sala (sala 2)

Matematicamente id_estado = 8*sala + 4*sujeira_sala1 + 2*sujeira_sala2 + sujeira_sala3
*/
//Uma enum para definir o status ABERTO (não explorado), FECHADO(já explorado), LACO(repetico), OBJETIVO(objetivo final)
enum StatusEstado {ABERTO = 0, FECHADO = 1, LACO = 2, OBJETIVO = 3};
// Agora o tipo de noh(e / ou)        
enum TipoNoh  {E = 0, OU = 1};
//O tipo da ação
enum Acao{ ESQUERDA = 0, DIREITA = 1, ASPIRAR = 2};
string str_acao[3] = {"ESQUERDA", "DIREITA", "ASPIRAR"};
// Declaração da classe. Os métodos são implementados posteriormente.
// A classe pra representar o Nó E/OU, com o pai(um nó estado), o tipo(E/OU) e seus filhos
class NohEstado;
class NohEOU{
    public:   
		//Atributos
		NohEstado* pai;
		TipoNoh tipo;
		Acao acao;
		vector<NohEstado *> filhos;
		int pendentes;
		NohEOU(NohEstado* pai, TipoNoh tipo, Acao acao, vector<NohEstado *> filhos);
		void atualiza();
		string to_str();
};

// Uma classe para representar um nó de estado
class NohEstado{
	public:
		// Atributos
		NohEOU *pai;
		int sala;
		vector<bool> ambiente;
		int id_estado;
		StatusEstado estado;
		vector<NohEOU *> filhos;
		
		// Construtor
		NohEstado(NohEOU *pai, int sala, vector<bool> ambiente){
			this->pai = pai;
			this->sala = sala;
			this->ambiente = ambiente;
			this->id_estado = calc_id();
			this->estado = gera_estado();
			this->filhos=vector<NohEOU *>();
		}

		// Calcula o id do estado baseado no que já foi explicado
		int calc_id(){
			return 8*this->sala+4*this->ambiente[0]+2*this->ambiente[1]+this->ambiente[2];
		}

		//Checa qual o id do estado
		StatusEstado gera_estado(){
			if( count(ambiente.begin(), ambiente.end(), true) ==0 ) // Não tem sujeira
				return OBJETIVO;
			NohEOU *ascendenteEOU = pai;
			NohEstado *ascendente = nullptr;
			bool eou=true;
			while (true){ //Visita todos os nós ascendentes para verificar se já repetiu
				if(!eou){
					if(ascendente==nullptr)
						break;
					if(id_estado == ascendente->id_estado)
						return LACO;
					ascendenteEOU = ascendente->pai;
					eou=true;
				}else{
					if(ascendenteEOU==nullptr)
						break;
					ascendente = ascendenteEOU->pai;
					eou=false;
				}
			}
			return ABERTO;
		}

		// Ação de mover para a esquerda, retorna uma cópia desse estado com o número da sala - 1 
		NohEOU * move_esquerda(){
			if(sala == 0)
				return nullptr;
			NohEOU *atual = new NohEOU(this, OU, ESQUERDA, vector<NohEstado *>());
			atual->filhos.push_back(new NohEstado(atual, sala-1, ambiente));
			atual->pendentes = 1;
			return atual;
		}

		// Ação de mover para a direita, retorna uma cópia desse estado com o número da sala + 1
		NohEOU * move_direita(){
			if(sala == 2)
				return nullptr;
			NohEOU *atual = new NohEOU(this, OU, DIREITA, vector<NohEstado *>());
			atual->filhos.push_back(new NohEstado(atual, sala+1, ambiente));
            atual->pendentes = 1;
			return atual;
		}

		// Ação de aspirar: se tiver sujo pode aspirar só o atual, um dos adjacentes ou os dois. Se tiver limpo pode deixar ou sujar
		NohEOU *aspira(){
			vector<NohEstado *> proximos;
			NohEOU *atual = new NohEOU(this, E, ASPIRAR, vector<NohEstado *>());
			if(ambiente[sala]){ // Tá sujo
				vector<vector<bool>> ambientes;
				vector<bool> amb;
				for(int i=0;i<3;i++)
					amb.push_back(i == sala?false:ambiente[i]); //Limpa só a sala atual
				ambientes.push_back(amb);
				if (sala>0 && ambiente[sala-1]) {
					vector<bool> amb2;
					for(int i=0;i<3;i++)
						amb2.push_back(sala-i==0||sala-i==1?false:ambiente[i]); //Limpa atual e esquerda
					ambientes.push_back(amb2);
				}             
				if(sala<2 && ambiente[sala+1]){
					vector<bool> amb2;
					for(int i=0;i<3;i++)
						amb2.push_back(i-sala==0||i-sala==1?false:ambiente[i]); //Limpa atual e direita
					ambientes.push_back(amb2);
                }
				if (sala==1 and count (ambiente.begin(), ambiente.end(), true)==3){
					vector<bool> amb2;
					for(int i=0;i<3;i++)
						amb2.push_back(false); // Limpa todos
					ambientes.push_back(amb2);
				}
				for(auto amb3 = ambientes.begin(); amb3!=ambientes.end(); amb3++)
					proximos.push_back(new NohEstado(atual, sala, *amb3));
			}else{
				proximos.push_back(new NohEstado(atual, sala, ambiente)); // mantem limpo
				proximos.push_back(new NohEstado(atual, sala, ambiente)); // mantem limpo
				(*(proximos.end()-1))->ambiente[sala]=true; // suja a sala
			}
			atual->filhos = proximos;
			atual->pendentes = proximos.size();
			return atual;
		}
		
		// Pegando todas as ações
		vector<NohEstado *> gera_acoes(){
			vector<NohEstado *> proximos;
			NohEOU *esquerda = move_esquerda();
			NohEOU *direita = move_direita();
			NohEOU *aspirador = aspira();
			if (esquerda != nullptr){
				proximos.insert(proximos.end(), esquerda->filhos.begin(), esquerda->filhos.end());
				filhos.push_back(esquerda);
			}
			if (direita != nullptr){
				proximos.insert(proximos.end(), direita->filhos.begin(), direita->filhos.end());
				filhos.push_back(direita);
			}
			proximos.insert(proximos.end(), aspirador->filhos.begin(), aspirador->filhos.end());
        	filhos.push_back(aspirador);
			return proximos;
		}

    // Quando todas as ações abaixo estão resolvidas, o nó esta fechado
    void atualiza(){
        for(auto filho: filhos)
            if(filho->pendentes != 0)
                return;
		if(pai != nullptr)
			pai->atualiza();
		else
			estado = FECHADO;
	}
    
    //Representando o estado como string
    string to_str(){
        string str = "Aspirador na sala " + to_string(sala) + ". Ambiente: ";
        for(auto a:ambiente)
			str+= a ? " Sujo ": " Limpo ";
        return str;
	}
};

NohEOU::NohEOU(NohEstado* pai, TipoNoh tipo, Acao acao, vector<NohEstado *> filhos){
	this->pai = pai;
	this->tipo = tipo;
	this->acao = acao;
	this->filhos = filhos;
	this->pendentes = 0;
}
		
// Toda vez que um nó tem todas as suas possibilidades verificadas
void NohEOU::atualiza(){
	pendentes--;
	if(pendentes == 0)
		pai->atualiza();
}

string NohEOU::to_str(){
	return "Nó " + to_string(tipo) + ": acao: " + str_acao[acao] + " filhos: " +  to_string(filhos.size()) + " pendentes: " + to_string(pendentes);
}

//Fazendo uma busca em largura na árvore
NohEstado *busca_em_largura(NohEstado *estado_inicial){
    NohEstado * atual = estado_inicial;
    queue<NohEstado *> fila;
    fila.push(atual);
    //R epete até que o estado inicial esteja fechado (tem solução) ou que a fila fique vazia(não tem solução)
    while(estado_inicial->estado !=FECHADO and fila.size()!=0){
        atual = fila.front();
        fila.pop();
        if(atual->estado==ABERTO){  //Se o nó está na lista aberta, expande
            vector<NohEstado *> proximos = atual->gera_acoes();
            for(auto proximo:proximos)
                fila.push(proximo);
        }else //Se não está, é porque ou é objetivo ou é um laço ou já foi fechado, então atualiza o status
            atual->atualiza();
    }
    if(estado_inicial->estado == FECHADO)
        return atual;
    return nullptr;
}

void exibe_solucao(NohEstado *atual, string caminho);

//Método que exibe a solução de forma recursiva
void exibe_solucao(NohEOU *atual, string caminho){
    for(NohEstado *filho:atual->filhos){ // Pega apenas as que levam a um subárvore com soluções
        if(filho->estado == OBJETIVO)
			cout << caminho << filho->to_str() << endl; // Exibe a solução se for objetivo
		if(filho->filhos.size() !=0)
			exibe_solucao(filho, caminho); // Chama recursivamente todos os filhos
	}
}

void exibe_solucao(NohEstado *atual, string caminho){
	for(NohEOU *filho:atual->filhos){ // Pega apenas as que levam a um subárvore com soluções
		if(filho->pendentes == 0)
			exibe_solucao(filho, caminho+str_acao[filho->acao] + "-->"); // Chama recursivamente se não tiver pendências
	}
}

int main(){
	vector<bool> ambiente = vector<bool>(3, true);
	NohEstado *estado_inicial = new NohEstado(nullptr, 1, ambiente);
	cout << "\n\nEstado inicial #1 : " << estado_inicial->to_str() << endl;
	NohEstado *solucao = busca_em_largura(estado_inicial);
	if(solucao!=nullptr)
		exibe_solucao(estado_inicial, "Solução:" );
	else
		cout << "Não encontrou solução!!!" << endl;

	ambiente[1]=false;
	delete estado_inicial;
	estado_inicial = new NohEstado(nullptr, 2, ambiente);
	cout << "\n\nEstado inicial #2 : " << estado_inicial->to_str() << endl;
	solucao = busca_em_largura(estado_inicial);
	if(solucao!=nullptr)
		exibe_solucao(estado_inicial, "Solução:" );
	else
		cout << "Não encontrou solução!!!" << endl;

	ambiente[1]=true;
	ambiente[2]=false;
	delete estado_inicial;
	estado_inicial = new NohEstado(nullptr, 2, ambiente);
	cout << "\n\nEstado inicial #3 : " << estado_inicial->to_str() << endl;
	solucao = busca_em_largura(estado_inicial);
	if(solucao!=nullptr)
		exibe_solucao(estado_inicial, "Solução:" );
	else
		cout << "Não encontrou solução!!!" << endl;

	ambiente[1]=false;
	ambiente[2]=true;
	delete estado_inicial;
	estado_inicial = new NohEstado(nullptr, 1, ambiente);
	cout << "\n\nEstado inicial #4 : " << estado_inicial->to_str() << endl;
	solucao = busca_em_largura(estado_inicial);
	if(solucao!=nullptr)
		exibe_solucao(estado_inicial, "Solução:" );
	else
		cout << "Não encontrou solução!!!" << endl;
	return 0;
}
