from enum import Enum
import queue

#O recomendado é implementar usando orientação a objetos com herança, mas pra essa primeira solução vou
#fazer algo mais simples, específico para resolver nosso problema.

#Para facilitar vamos definir a configuração do ambiente de 0 a 7, como em uma codificação binária para 
#sujeira. Se for 0 é limpa, então 000 são todos as salas limpas, ja 111 são todas as salas sujas. Isso nos
#dá 8 cenários. Nesse caso podemos convencionar
# 0 a 7 quando o robô está na 1ª sala (sala 0)
#8 a 15 quando o robô está na 2ª sala (sala 1)
#16 a 23 quando o robô está na 3ª sala (sala 2)
#Matematicamente id_estado = 8*sala + 4*sujeira_sala1 + 2*sujeira_sala2 + sujeira_sala3

#Uma classe para definir o status ABERTO (não explorado), FECHADO(já explorado), LACO(repetico), OBJETIVO(objetivo final)
class StatusEstado(Enum):
    ABERTO = 0
    FECHADO = 1
    LACO = 2
    OBJETIVO = 3

#Uma classe para representar um nó de estado
class NohEstado:

    def __init__(self, pai, sala, ambiente):
        self.pai = pai
        self.sala = sala
        self.ambiente = ambiente
        self.id_estado = self.calc_id()
        self.estado = self.gera_estado()
        self.filhos=[]

    #calcula o id do estado baseado no que já foi explicado
    def calc_id(self):
        return 8*self.sala+4*self.ambiente[0]+2*self.ambiente[1]+self.ambiente[2]

    #Checa qual o id do estado
    def gera_estado(self):
        if self.ambiente.count(True)==0: #Não tem sujeira
            return StatusEstado.OBJETIVO
        else:
            ascendente = self.pai
            while ascendente != None: #Visita todos os nós ascendentes para verificar se já repetiu
                if isinstance(ascendente, NohEstado) and self.id_estado == ascendente.id_estado:
                    return StatusEstado.LACO
                ascendente = ascendente.pai
        return StatusEstado.ABERTO

    #Ação de mover para a esquerda, retorna uma cópia desse estado com o número da sala - 1 
    def move_esquerda(self):
        if self.sala == 0:
            return None
        atual = NohEOu(self, TipoNoh.OU, Acao.ESQUERDA, None)
        atual.filhos = [NohEstado(atual, self.sala-1, self.ambiente.copy())]
        atual.pendentes = 1
        return atual

    #Ação de mover para a direita, retorna uma cópia desse estado com o número da sala + 1
    def move_direita(self):
        if self.sala == 2:
            return None
        atual = NohEOu(self, TipoNoh.OU, Acao.DIREITA, None)
        atual.filhos = [NohEstado(atual, self.sala+1, self.ambiente.copy())]
        atual.pendentes = 1
        return atual

    #Ação de aspirar: se tiver sujo pode aspirar só o atual, um dos adjacentes ou os dois. Se tiver limpo pode deixar ou sujar
    def aspira(self):
        proximos = []
        atual = NohEOu (self, TipoNoh.E, Acao.ASPIRAR, None)
        if self.ambiente[self.sala]==True: # Tá sujo
            ambientes=[]
            ambientes.append([False if i == self.sala else self.ambiente[i] for i in range(3)]) #Limpa só a sala atual
            if self.sala>0 and self.ambiente[self.sala-1]==True :
                ambientes.append([False if self.sala-i in [0,1] else self.ambiente[i] for i in range(3)]) #Limpa atual e esquerda
            if self.sala<2 and self.ambiente[self.sala+1]==True:
                ambientes.append([False if i-self.sala in [0,1] else self.ambiente[i] for i in range(3)]) #Limpa atual e direita
            if self.sala==1 and self.ambiente.count(True)==3:
                ambientes.append([False]*3) #Limpa todos
            proximos = [NohEstado(atual, self.sala, ambiente) for ambiente in ambientes]
        else:
            proximos = [NohEstado(atual, self.sala, self.ambiente.copy()) for i in range(2)]
            proximos[1].ambiente[self.sala]=True # Tá limpo
        atual.filhos = proximos
        atual.pendentes = len(proximos)
        return atual
       
    #Pegando todas as ações
    def gera_acoes(self):
        proximos=[]
        esquerda = self.move_esquerda()
        direita = self.move_direita()
        aspirador = self.aspira()
        if esquerda != None:
            proximos.extend(esquerda.filhos)
            self.filhos.append(esquerda)
        if direita != None:
            proximos.extend(direita.filhos)
            self.filhos.append(direita)
        proximos.extend(aspirador.filhos)
        self.filhos.append(aspirador)
        return proximos

    #Quando todas as ações abaixo estão resolvidas, o nó esta fechado
    def atualiza(self):
        for filho in self.filhos:
            if filho.pendentes != 0:
                break
        else:
            if self.pai != None:
                self.pai.atualiza()
            else:
                self.estado = StatusEstado.FECHADO
    
    #Representando o estado como string
    def __str__(self):
        a = ['Sujo' if e else 'Limpo' for e in self.ambiente]
        return f'Aspirador na sala {self.sala}. Ambiente: {a}'

#Agora o tipo de noh(e / ou)        
class TipoNoh(Enum):
    E = 0
    OU = 1

#O tipo da ação
class Acao(Enum):
    ESQUERDA = 'ESQUERDA'
    DIREITA = 'DIREITA'
    ASPIRAR = 'ASPIRAR'


#A classe pra representar o Nó E/OU, com o pai(um nó estado), o tipo(E/OU) e seus filhos
class NohEOu:
    
    def __init__(self, pai, tipo, acao, filhos):
        self.pai = pai
        self.tipo = tipo
        self.acao = acao
        self.filhos = filhos
        self.pendentes = 0

    #Toda vez que um nó tem todas as suas possibilidades verificadas
    def atualiza(self):
        self.pendentes -= 1
        if self.pendentes == 0:
            self.pai.atualiza()

    def __str__(self):
        return "Nó " + self.tipo.value + ": acao: " + self.acao.value + " filhos: " + len(self.filhos) + " pendentes: " + self.pendentes

#Fazendo uma busca em largura na árvore
def busca_em_largura(estado_inicial):
    atual = estado_inicial
    fila = queue.Queue()
    fila.put(atual)
    #Repete até que o estado inicial esteja fechado (tem solução) ou que a fila fique vazia(não tem solução)
    while estado_inicial.estado != StatusEstado.FECHADO and fila.qsize()!=0:
        atual = fila.get()
        if atual.estado == StatusEstado.ABERTO: #Se o nó está na lista aberta, expande
            proximos = atual.gera_acoes()
            for proximo in proximos:
                fila.put(proximo)
        else: #Se não está, é porque ou é objetivo ou é um laço ou já foi fechado, então atualiza o status
            atual.atualiza()
    if estado_inicial.estado == StatusEstado.FECHADO:
        return atual
    return None

#Método que exibe a solução de forma recursiva
def exibe_solucao(atual, caminho):
    for filho in atual.filhos:  # Pega apenas as que levam a um subárvore com soluções
        if isinstance(filho, NohEOu) and filho.pendentes == 0:
            exibe_solucao(filho,caminho+filho.acao.value + '-->') # Chama recursivamente se não tiver pendências
        elif isinstance(filho, NohEstado):
            if filho.estado == StatusEstado.OBJETIVO:
                print(caminho,filho) # Exibe a solução se for objetivo
            if len(filho.filhos)!=0:
                exibe_solucao(filho, caminho) # Chama recursivamente todos os filhos

    

estado_inicial = NohEstado(None, 1, [True, True, True])
print('\n\nEstado inicial #1', estado_inicial)
busca_em_largura(estado_inicial)
exibe_solucao(estado_inicial, 'Solução:' )

estado_inicial = NohEstado(None, 2, [True, False, False])
print('\n\nEstado inicial #2', estado_inicial)
busca_em_largura(estado_inicial)
exibe_solucao(estado_inicial, 'Solução:' )

estado_inicial = NohEstado(None, 2, [True, True, False])
print('\n\nEstado inicial #3', estado_inicial)
busca_em_largura(estado_inicial)
exibe_solucao(estado_inicial, 'Solução:' )

estado_inicial = NohEstado(None, 1, [True, False, True])
print('\n\nEstado inicial #4', estado_inicial)
busca_em_largura(estado_inicial)
exibe_solucao(estado_inicial, 'Solução:' )
