#Coloquei todo o código em um único arquivo, mas o interessante é que fossem separados como módulos
#Como é apenas um exemplo, não me preocupei muito com o desempenho também

#Usaremos a heapq, que é uma implementação de fila de prioridades em python sobre uma estrutura heap
from heapq import *

#Uma classe para representar o labirinto
class Labirinto:
    #Construtor, todo labirinto tem um número de linhas, colunas, posicoes inicial e final, e as posicoes dos obstaculos
    def __init__(self, linhas, colunas, posicao_inicial, posicao_final, obstaculos):
        self.linhas = linhas
        self.colunas = colunas
        self.posicao_inicial = posicao_inicial
        self.posicao_final = posicao_final
        self.obstaculos = obstaculos
        self.solucao=[]
    
    #Transformando em string    
    def __str__(self):
        lab=''
        for i in range(self.linhas):
            for j in range(self.colunas):
                if (i,j) in self.solucao:
                    lab+='X'
                elif (i,j) == self.posicao_inicial:
                    lab+='I'
                elif (i,j) == self.posicao_final: 
                    lab+='F'
                elif (i,j) in self.obstaculos:
                    lab+='O'
                else:
                    lab+='_'
            lab+='\n'
        return lab

#Uma classe nó, para guardar cada solução
class Noh:
    #Construtor que inicia cada nó com o pai, a ação que levou até ele, o estado e o valor g (h é estimado)
    def __init__(self, labirinto, pai, acao, estado, g):
        self.labirinto = labirinto
        self.pai = pai
        self.estado = estado # o estado é a célula em que o personagem se encontra
        self.acao = acao
        self.g = g
        self.h = self.calc_h()
        self.f = self.g+self.h

    # Custo estimado pela distância manhattan
    def calc_h(self):
        x1 = self.estado[0]
        y1 = self.estado[1]
        x2 = self.labirinto.posicao_final[0]
        y2 = self.labirinto.posicao_final[1]
        return abs(x1-x2)+abs(y1-y2)

    # Função para expandir os nós a partir da posição atual
    def expande(self):
        novos = []
        # offset é o vetor de deslocamento em x,y para cada movimento
        for offset, movimento in zip([[0,1],[1,0],[0,-1],[-1,0]], ['direita', 'baixo','esquerda','cima']):
            l = self.estado[0]+offset[0]
            c = self.estado[1]+offset[1]
            if l>=0 and l<self.labirinto.linhas and c>=0 and c<self.labirinto.colunas and not (l,c) in self.labirinto.obstaculos:
                novos.append(Noh(self.labirinto, self, movimento, (l,c), self.g+1))
        return novos

    # Funções auxiliares para ordenar o nó na fila do python
    def __lt__(self, other):
        return self.f < other.f

    def __eq__(self, other):
        return other!=None and self.f == other.f

    def __str__(self):
        return str(self.labirinto)

#O algoritmo A* implementado sobre a heap
def A_estrela(labirinto):
    #Uma lista aberta vazia
    lista_aberta = []
    #Uma lista fechada como uma matriz com todos as celulas False, indicando que não chegou até lá
    lista_fechada = [ [False for coluna in range(labirinto.colunas)] for linha in range(labirinto.linhas)]
    #Nó inicial, não tem pai, nem ação. O estado é a posição inicial com custo zero
    raiz=Noh(labirinto, None, 'Estado inicial', labirinto.posicao_inicial, 0)
    #Insere a raiz na lista aberta
    heappush(lista_aberta, raiz)
    while len(lista_aberta)!=0:
        #Pega o melhor da lista aberta
        proximo=heappop(lista_aberta)
        #Se h==0  então chegou no objetivo
        if proximo.h==0:
            imprime_solucao(proximo)
            return
        #Se o nó atual já foi visitado, continua com o proximo
        proxima_linha = proximo.estado[0]
        proxima_coluna = proximo.estado[1]
        if lista_fechada [proxima_linha][proxima_coluna] == True:
            continue
        #Senão, marca o próximo na lista fechada e adiciona na lista aberta
        lista_fechada [proxima_linha][proxima_coluna] = True
        novos = proximo.expande()
        for novo in novos:
            heappush(lista_aberta, novo)
    #Quando não há mais nós na lista aberta não há solução
    print('Não há solução')

#Função que imprime a solução, a partir do nó final vai procurando o pai até o estado inicial
def imprime_solucao(final):
    print('SOLUÇÃO FINAL')
    caminho=[]
    proximo=final
    labirinto = final.labirinto
    while proximo!=None:
        caminho.append(proximo)
        proximo=proximo.pai
    for atual in reversed(caminho):
        labirinto.solucao.append( (atual.estado[0],atual.estado[1]) )
        print(f'Movimento: {atual.acao}')
        print(f'Estado:\n{atual}')


lab1 = Labirinto(5, 5, (2,0), (3,4), [(1,1), (2,1), (4,1), (3,2), (2,4)])
A_estrela(lab1)

