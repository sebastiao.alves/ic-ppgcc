#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <iomanip>
#include <queue>

using namespace std;

// Constantes para indicar o deslocamento em x,y e os tipos de movimentos
const int offset[4][2] = {{0,1},{1,0},{0,-1},{-1,0}};
const string movimento[5] = {"direita", "baixo", "esquerda", "cima", "-"};

// Nesse exemplo em C++ vou colocar o labirinto como uma matriz de caractere
// I indica a posição inicial, F a posição final, O os obstaculos e _ espaços vazios
const int LINHAS=5, COLUNAS=5;
char labirinto[LINHAS][COLUNAS+1] = 
	{ { '_','_','_','_','_' ,'\0'},
	  { '_','O','_','_','_' ,'\0'},
	  { 'I','O','_','_','O','\0'},
	  { '_','_','O','_','F','\0'},
	  { '_','O','_','_','_','\0'}};

// Definição do nó para cada solução parcial, com referência ao nó pai, à ação que levou até ele
// O estado (posição x, y) e os valores de f, g e h

using Estado=pair<int,int>;
const Estado INICIO(2,0), FINAL(3,4);

class Noh{
	// Como é apenas um exemplo, vou deixar tudo público
	public:
		Noh *pai;
		string acao;
		Estado estado;
		int f, g, h;

		// Construtor passando as informações. A função h é estimada não precisa passar.
		Noh(Noh* pai, const string acao, Estado estado, int g){
			this->pai=pai;
			this->acao=acao;
			this->estado = estado;
			this->g=g;
			this->h=calc_h();
			this->f=this->g+this->h;
		}

		// Função heurísitca dada pela distância manhattan
		int calc_h(){
			int x1=estado.first, y1=estado.second, x2=FINAL.first, y2=FINAL.second;
			return abs(x1-x2) + abs(y1-y2);
		}

		//Função de expansão dos nós. Pega o estado atual, verifica se é possível fazer um dos 4 
		//movimentos permitidos e inserve em um vetor que é retornado ao final
		vector<Noh *> expande(){
			vector<Noh *> novos;
			int x_0=estado.first, y_0=estado.second;
			for(size_t i=0;i<4;i++){
				int l = x_0+offset[i][0], c=y_0+offset[i][1];
			          if(l>=0 && l<LINHAS && c>=0 && c<COLUNAS && labirinto[l][c]!='O'){
					Estado proximo(l,c);
					Noh *novo=new Noh(this, movimento[i], proximo, this->g+1);
					novos.push_back(novo);
				}
			}
		        return novos;
		}
};

// Uma estrutura auxiliar para comparar os nós para inserção na fila da lista aberta
struct CompNoh{
	bool operator()( const Noh* a, const Noh* b ) const {
		return a->f > b->f;
	}
};

// Definição de funções auxiliares (implementados abaixo)
void imprime_solucao(Noh *final);
void desaloca(priority_queue<Noh*, vector<Noh*>, CompNoh> lista_aberta);

//O algoritmo A* em si
void A_estrela(){
	//Definição da lista aberta (uma fila com prioridade) e lista fechada uma matriz de booleano
	priority_queue<Noh*, vector<Noh*>, CompNoh> lista_aberta;
	bool lista_fechada[LINHAS][COLUNAS]={false};
	//Inicia a lista aberta com o estado inicial
	Noh *raiz=new Noh(nullptr, "", INICIO, 0);
	lista_aberta.push(raiz);
	while(lista_aberta.size()!=0){
		Noh* proximo=lista_aberta.top();
		lista_aberta.pop();
		//Para quando o estado atual tem h==0, ou seja, chegou no objetivo
		if(proximo->h==0){
			imprime_solucao(proximo);
			desaloca(lista_aberta);
			return;
		}
		//Se o próximo já está na lista fechada, passa para o próximo
		if(lista_fechada[proximo->estado.first][proximo->estado.second]==true)
			continue;
		//Senão, insere ele na lista fechada e expande os próximos estados
		lista_fechada[proximo->estado.first][proximo->estado.second]=true;
		auto novos =  proximo->expande();
		for(auto novo:novos){
			lista_aberta.push(novo);
		}
	}
	//Se a lista aberta fica vazia sem chegar ao objetivo não há solução
	cout << "Não há solução!!" << endl;
}

//Apenas exibe o labirinto
void imprime_labirinto(){
	for(size_t i = 0; i<LINHAS; i++)
		cout << labirinto[i] << endl;
}

//Imprime a solução final passo a passo
void imprime_solucao(Noh *final){
	//Cria um vetor para os nós no caminho para a solução, partindo do estado final
	vector<Noh *> caminho;
	auto proximo=final;
	while (proximo!=nullptr){
		caminho.push_back(proximo);
		proximo=proximo->pai;
	}
	//Agora percorre ao contrário para mostrar a ordem do início para o fim
	for(auto atual=caminho.rbegin(); atual!=caminho.rend(); atual++){
		cout << "Movimento: " << (*atual)->acao << endl;
		labirinto[(*atual)->estado.first][(*atual)->estado.second]='X';
		imprime_labirinto();
	}
}

//Desalocando a memória
void desaloca(priority_queue<Noh*, vector<Noh*>, CompNoh> lista_aberta){
	while(!lista_aberta.empty()){
		delete lista_aberta.top();
		lista_aberta.pop();
	}
}

int main(){
	A_estrela();
	return 0;
}
