// Nesta solução em C++, para facilitar o armazenamento das soluções já visitadas, cada configuração será
// representada como um vetor com 12 posições, representando cada linha com 4 colunas

#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <queue>
#include <set>

using namespace std;

// Constantes para indicar o deslocamento em x,y e os tipos de movimentos
const int offset[4][2] = {{0,1},{1,0},{0,-1},{-1,0}};
const string movimento[5] = {"esquerda", "cima", "direita", "baixo"};

// Definição do nó para cada solução, com referência ao nó pai, à ação que levou até ele
// O estado (um vetor de caracteres com a posição atual de todos) e os valores de f, g e h
class Noh{
	// Como é apenas um exemplo, vou deixar tudo público
	public:
		Noh *pai;
		string acao;
		vector<char> estado;
		int f, g, h;

		// Construtor passando as informações. A função h é estimada não precisa passar.
		Noh(Noh* pai, string acao, vector<char> estado, int g){
			this->pai=pai;
			this->acao=acao;
			this->estado = estado;
			this->g=g;
			this->h=calc_h();
			this->f=this->g+this->h;
		}

		// Função heurísitca dada pela soma da distância euclidiana de cada peça ao seu lugar
		int calc_h(){
			int soma=0;
			for(size_t i=0;i<3;i++)
				for(size_t j=0;j<4;j++){
					int numero = this->estado[i*4+j];
					soma+=dist_euclidiana(i, j, numero/4, numero%4);
				}
			return soma;
		}

		int dist_euclidiana(int x1,int y1,int x2,int y2){
			return (int) ceil(sqrt(pow(x1-x2,2) + pow(y1-y2,2)));
		}

		//Função de expansão dos nós. Pega o estado atual, verifica se é possível fazer um dos 4 
		//movimentos permitidos e inserve em um vetor que é retornado ao final
		vector<Noh *> expande(){
			vector<Noh *> novos;
			// Acha onde está o espaço vazio
			size_t x_0, y_0;
			for(size_t i=0;i<12;i++)
				if(this->estado[i]==0){
					x_0=i/4; y_0=i%4;
				}
			// Para cada movimento possível, faz o deslocamento da peça e insere no vetor
			for(size_t i=0;i<4;i++){
				size_t l = x_0+offset[i][0], c=y_0+offset[i][1];
			          if(l>=0 && l<3 && c>=0 && c<4){
					string jogada = "Move " + to_string(this->estado[l*4+c]) + " para " + movimento[i];
					vector<char> novoEstado=this->estado;
					swap(novoEstado[x_0*4+y_0], novoEstado[l*4+c]);
					Noh *novo=new Noh(this, jogada, novoEstado, this->g+1);
					novos.push_back(novo);
				}
			}
		        return novos;
		}

		// Função para exibir o estado atual
		void imprime(){
			for(size_t i=0;i<3;i++){
				for(size_t j=0;j<4;j++)
					cout << setw(2) << int(this->estado[i*4+j]) << " ";
				cout << endl;
			}
//			cout << "f=" << this->g << " + " << this->h << " = " << this->f << endl;
		}
};

// Uma estrutura auxiliar para comparar os nós para inserção na fila da lista aberta
struct CompNoh{
	bool operator()( const Noh* a, const Noh* b ) const {
		return a->f > b->f;
	}
};

//Definição da lista aberta (uma fila com prioridade) e lista fechada (um conjunto de configurações 
//representadas como vetor de char)
priority_queue<Noh*, vector<Noh*>, CompNoh> lista_aberta;
set<vector<char>> lista_fechada;
void imprime_solucao(Noh *final);


// O algoritmo A* em si
void A_estrela(vector<char> estado_inicial){
	// Nó raiz é criado com o estado inicial
	Noh *raiz=new Noh(nullptr, "", estado_inicial, 0);
	lista_aberta.push(raiz);
	while(lista_aberta.size()!=0){
		//Pega o próximo da lista aberta
		Noh* proximo=lista_aberta.top();
		lista_aberta.pop();
		//Para quando o estado atual tem h==0, ou seja, chegou no objetivo
		if(proximo->h==0){
			imprime_solucao(proximo);
			return;
		}
		//Se o próximo já está na lista fechada, passa para o próximo	
		if(lista_fechada.find(proximo->estado)!=lista_fechada.end())
			continue;
		//Senão, insere ele na lista fechada e expande os próximos estados
		lista_fechada.insert(proximo->estado);
		auto novos =  proximo->expande();
		for(auto novo:novos){
			lista_aberta.push(novo);
		}
	}
	//Se a lista aberta fica vazia sem chegar ao objetivo não há solução
	cout << "Não há solução!!" << endl;
}

//Imprime a solução final passo a passo
void imprime_solucao(Noh *final){
	//Cria um vetor para os nós no caminho para a solução, partindo do estado final
	vector<Noh *> caminho;
	auto proximo=final;
	while (proximo!=nullptr){
		caminho.push_back(proximo);
		proximo=proximo->pai;
	}
	//Agora percorre ao contrário para mostrar a ordem do início para o fim
	for(auto atual=caminho.rbegin(); atual!=caminho.rend(); atual++){
		cout << (*atual)->acao << endl;
		(*atual)->imprime();
	}
	cout << "Total de movimentos: " << caminho[0]->g << endl;
}

int main(){
	// O estado initial trocou apenas a primeira posição com a última
	vector<char> estado_inicial = {11,1,2,3,4,5,6,7,8,9,10,0};

	// Esse estado abaixo troca várias posições, gera vários estados e movimentos.
	// O meu computador trava antes de encontrar a solução. Tente executar no seu,
	// Comentando a linha acima e descomentando a linha abaixo.
//	vector<char> estado_inicial = {11,3,1,9,10,5,8,0,4,2,7,6};
	A_estrela(estado_inicial);

	//Desalocando memória
	while(!lista_aberta.empty()){
		delete lista_aberta.top();
		lista_aberta.pop();
	}
	return 0;
}
