#Coloquei todo o código em um único arquivo, mas o interessante é que fossem separados como módulos
#Como é apenas um exemplo, não me preocupei muito com o desempenho também

#Usaremos a heapq, que é uma implementação de fila de prioridades em python sobre uma estrutura heap
#Também usaremos um SortedSet para guardar as configurações da lista fechada
import math
from heapq import *
from sortedcollections import SortedSet

#Uma classe nó, para guardar cada solução
class Noh:

    #Construtor que inicia cada nó com o pai, a ação que levou até ele, o estado e o valor g (h é estimado)
    def __init__(self, pai, acao, estado, g):
        self.pai=pai
        self.estado=estado
        self.acao=acao
        self.g=g
        self.h=self.calc_h()
        self.f=self.g+self.h
        self.hash_code=hash(str(estado))

    # Custo estimado pela soma das distâncias euclidianas de uma peça a sua posição
    def calc_h(self):
        soma=0
        for linha in range(3):
            for coluna in range(4):
                numero=self.estado[linha][coluna]
                soma+=Noh.dist_euclidiana(linha, coluna, numero//4, numero%4)
        return soma

    def dist_euclidiana(x1, y1, x2, y2):
        return math.ceil(math.sqrt((x1-x2)**2 + (y1-y2)**2))

    # Função para expandir os nós a partir da posição atual
    def expande(self):
        vazio=[[a,b] for a in range(3) for b in range(4) if self.estado[a][b]==0][0]
        novos = []
        # offset é o vetor de deslocamento em x,y para cada movimento
        for offset, movimento in zip([[0,1],[1,0],[0,-1],[-1,0]], ['esquerda', 'cima','direita','baixo']):
            l=vazio[0]+offset[0]
            c=vazio[1]+offset[1]
            if l>=0 and l<3 and c>=0 and c<4:
                prox_estado = list(map(list, self.estado))
                prox_estado[vazio[0]][vazio[1]], prox_estado[l][c] = prox_estado[l][c], prox_estado[vazio[0]][vazio[1]]
                novos.append(Noh(self, f'{self.estado[l][c]} para {movimento}', prox_estado, self.g+1))
        return novos

# Algumas funções auxiliares para ordenação das soluções na heap (lt e eq) e conversão para string(str e repr)
    def __lt__(self, other):
        return self.f < other.f

    def __eq__(self, other):
        return other!=None and self.f == other.f

    def __str__(self):
        return '\n'.join([str(linha) for linha in self.estado])

    def __repr__(self):
        return '\n'.join([str(linha) for linha in self.estado])

#Uma lista aberta como list e uma lista fechada como um conjunto ordenado
lista_aberta = []
lista_fechada = SortedSet()

#O algoritmo A* implementado sobre a heap
def A_estrela(estado_inicial):
    #Nó inicial, não tem pai, nem ação. O estado é a posição inicial com custo zero
    raiz=Noh(None, None, estado_inicial,0)
    #Insere a raiz na lista aberta
    heappush(lista_aberta, raiz)
    while len(lista_aberta)!=0:
        #Pega o melhor da lista aberta
        proximo=heappop(lista_aberta)
        #Se h==0  então chegou no objetivo
        if proximo.h==0:
            imprime_solucao(proximo)
            return
        #Se o nó atual já foi visitado, continua com o proximo
        if proximo.hash_code in lista_fechada:
            continue
        #Senão, marca o próximo na lista fechada e adiciona na lista aberta
        lista_fechada.add(proximo.hash_code)
        novos = proximo.expande()
        for novo in novos:
            if not novo.hash_code in lista_fechada:
               heappush(lista_aberta, novo)
    #Quando não há mais nós na lista aberta não há solução
    print('Não há solução')

#Função que imprime a solução, a partir do nó final vai procurando o pai até o estado inicial
def imprime_solucao(final):
    caminho=[]
    proximo=final
    while proximo!=None:
        caminho.append(proximo)
        proximo=proximo.pai
    for atual in reversed(caminho):
        print(f'Movimento: {atual.acao}')
        print(f'Estado:\n{atual}')
    print(f'Total de movimentos: {final.g}')


#Executando o código

#O estado initial trocou apenas a primeira posição com a última
estado_inicial=[[11,1,2,3],[4,5,6,7],[8,9,10,0]];

#Esse estado abaixo troca várias posições, gera vários estados e movimentos.
#O meu computador trava antes de encontrar a solução. Tente executar no seu,
# Comentando a linha acima e descomentando a linha abaixo.
#estado_inicial=[[11,3,1,9],[10,5,8,0],[4,2,7,6]]

A_estrela(estado_inicial)

