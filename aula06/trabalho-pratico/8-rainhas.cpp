#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <tuple>

using namespace std;
/*
# A resolução do problema consiste em um algoritmo recursivo com retrocesso (backtracking) com consistência de arco.
# Admitimos que o tabuleiro tem as posições numeradas de 1 a 64, onde a primeira linha tem a numeração de 1 a 8, 
# a segunda de 9 a 16 e assim por diante. Usaremos uma lista de adjacência para representar o grafo de restrições,
# onde as restrições terão apenas as casas das colunas posteriores. 
# Para o backtracking cada casa contará com quantas casas está em conflito no momento atual para testar a consistência,
# sendo atualizado a cada progresso (+1) e retorno (-1)
*/

typedef tuple<int, int> casa;

 map<casa, vector<casa>> gera_grafo_restricoes(){
    map<casa, vector<casa>> grafo;
    for(int l=0;l<8;l++){
		for(int c=0;c<8;c++){
            vector<casa> vizinhos;
            for(int col=c+1;col<8;col++) //adiciona os da mesma linha, com colunas diferentes
                vizinhos.push_back(make_tuple(l,col));
            for(int lin=l-1, col=c+1; lin>=0 && col<8; lin--, col++) //adiciona os da diagonal acima dele
                vizinhos.push_back(make_tuple(lin, col));
            for(int lin=l+1, col=c+1; lin<8 && col<8; lin++, col++) //adiciona os da diagonal abaixo dele
                vizinhos.push_back(make_tuple(lin, col));
            grafo.insert(make_pair(make_tuple(l,c), vizinhos));
		}
	}
    return grafo;
}

// O grafo com todas as restrições
map<casa, vector<casa>> grafo_restricoes;
// Uma memória para guardar a consistência do nó. Um valor acima de 0 significa que há inconsistência com ele 
int restricoes [8][8] = {0};

void atualiza_restricao(int linha, int coluna, bool incrementa){ // Adiciona(ou remove) as restriçoes relacionadas àquela posição
	int l,c;
    for(casa restricao:grafo_restricoes.at(make_tuple(linha,coluna))){
		tie(l,c) = restricao;
        if(incrementa)
            restricoes[l][c]++;
        else
            restricoes[l][c]--;
	}
}

// Agora a função de backtracking em si
void resolve_rainhas(int coluna=0, vector<int> solucao=vector<int>()){
	static int solucoes = 0;
    for(int linha=0;linha<8;linha++){
        if(restricoes[linha][coluna]==0){
            solucao.push_back(linha);
            if(coluna == 7){
				solucoes++;
				cout << "Solução #" << solucoes << ": ";
                for_each(solucao.begin(), solucao.end(),  [] (int pos) {cout << pos << " ";});
                cout << endl;
            }else{
                atualiza_restricao(linha, coluna, true);
                resolve_rainhas(coluna+1, solucao);
                atualiza_restricao(linha, coluna, false);
			}
            solucao.pop_back();
		}
	}
}

int main(){
	grafo_restricoes = gera_grafo_restricoes();
	resolve_rainhas();
	return 0;
}
