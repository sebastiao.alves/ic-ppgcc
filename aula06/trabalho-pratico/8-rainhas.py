
# A resolução do problema consiste em um algoritmo recursivo com retrocesso (backtracking) com consistência de arco.
# Admitimos que o tabuleiro tem as posições numeradas de 1 a 64, onde a primeira linha tem a numeração de 1 a 8, 
# a segunda de 9 a 16 e assim por diante. Usaremos uma lista de adjacência para representar o grafo de restrições,
# onde as restrições terão apenas as casas das colunas posteriores. 
# Para o backtracking cada casa contará com quantas casas está em conflito no momento atual para testar a consistência,
# sendo atualizado a cada progresso (+1) e retorno (-1)

def gera_grafo_restricoes():
    grafo = {}
    for l in range(8):
        for c in range(8):
            vizinhos = []
            for col in range(c+1,8): #adiciona os da mesma linha, com colunas diferentes
                vizinhos.append((l,col))
            for lin,col in zip(range(l-1, -1, -1), range(c+1, 8)):  # adiciona os da diagonal acima dele
                vizinhos.append((lin, col))
            for lin,col in zip(range(l+1, 8), range(c+1, 8)): # adiciona os da diagonal abaixo dele
                vizinhos.append((lin, col))
            grafo[(l,c)]=vizinhos
    return grafo

# O grafo com todas as restrições
grafo_restricoes = gera_grafo_restricoes()
# Uma memória para guardar a consistência do nó. Um valor acima de 0 significa que há inconsistência com ele 
restricoes = [[0 for i in range(8)] for j in range(8)]

def atualiza_restricao(linha, coluna, incrementa): # Adiciona(ou remove) as restriçoes relacionadas àquela posição
    for l,c in grafo_restricoes[(linha,coluna)]:
        if incrementa:
            restricoes[l][c]+=1
        else:
            restricoes[l][c]-=1

# Agora a função de backtracking em si
def resolve_rainhas(coluna=0, solucao=[]):
    for linha in range(8):
        if restricoes[linha][coluna]==0:
            solucao.append(linha)
            if coluna == 7:
                print(solucao)
            else:
                atualiza_restricao(linha, coluna, True)
                resolve_rainhas(coluna+1, solucao)
                atualiza_restricao(linha, coluna, False)
            del solucao[coluna]

resolve_rainhas()
