import random

# Sobre as divisas temos o seguinte para cada estado
# MA: PI
# PI: MA, CE, PE, BA
# CE: PI, RN, PB, PE
# RN: PB, CE
# PB: CE, RN, PE
# PE: BA, PI, AL, CE, PB
# AL: SE, PE, BA
# SE: BA, AL
# BA: PI, PE, AL SE

# Podemos usar uma matriz de adjacência booleana para a implementação. O índice será de acordo com a ordem dada:
#              MA     PI     CE     RN     PB     PE     AL     SE     BA
limites = [ [False,  True, False, False, False, False, False, False, False],   # MA
            [ True, False,  True, False, False,  True, False, False,  True],   # PI
            [False,  True, False,  True,  True,  True, False, False, False],   # CE
            [False, False,  True, False,  True, False, False, False, False],   # RN
            [False, False,  True,  True, False,  True, False, False, False],   # PB
            [False,  True,  True, False,  True, False,  True, False,  True],   # PE
            [False, False, False, False, False,  True, False,  True,  True],   # AL
            [False, False, False, False, False, False,  True, False,  True],   # SE
            [False,  True, False, False, False,  True,  True,  True, False]]   # BA

#Domínio: 3 cores
cores = ["Vermelho", "Azul", "Verde"]
estados = ["MA", "PI", "CE", "RN", "PB", "PE", "AL", "SE", "BA"]

#Solução inicial aleatória
solucao_inicial = [random.choice(cores) for i in range(9) ]

#Função para calcular o número de conflitos
def conta_conflitos(solucao):
    conflitos=0
    conflitantes = set()
    for estado1 in range(9):
        for estado2 in range(estado1+1,9):
            #Testa, para cada estado, se eles tem cores iguais e fazem fronteira ao mesmo tempo
            if solucao[estado1] == solucao[estado2] and limites[estado1][estado2]:
                conflitos += 1
                conflitantes.add(estado1)
                conflitantes.add(estado2)
    return conflitos, list(conflitantes)

#Uma função para mostrar a solução final
def mostra_solucao(solucao):
    print("Solução final")
    for i,estado in zip(range(9), estados):
        print(estado,": ", solucao[i])
    print("Número de conflitos: ", conta_conflitos(solucao)[0])

#A heurística de conflitos mínimos em si
def conflitos_minimos(solucao_inicial, max_etapas):
    atual = solucao_inicial
    for iteracao in range(max_etapas):
        conflitos,conflitantes = conta_conflitos(atual)
        if conflitos == 0:
            print("O algoritmo encontrou a resposta!!!")
            break
        #Escolhe uma variável em conflito e muda seus valores, escolhendo o de conflitos mínimos
        var_conflito = random.choice(conflitantes)
        vizinhos, conf_vizinhos = [],[]
        for i in range(len(cores)):
            copia = atual.copy()
            copia[var_conflito]=cores[i]
            vizinhos.append(copia)
            conf_vizinhos.append(conta_conflitos(copia)[0])
        atual = vizinhos [ conf_vizinhos.index(min(conf_vizinhos)) ]
    return atual


solucao_final = conflitos_minimos(solucao_inicial, 1000)
mostra_solucao(solucao_final)



