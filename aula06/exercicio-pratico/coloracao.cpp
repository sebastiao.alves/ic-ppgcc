#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <ctime>
#include <tuple>

using namespace std;

/*
 *  Sobre as divisas temos o seguinte para cada estado
# MA: PI
# PI: MA, CE, PE, BA
# CE: PI, RN, PB, PE
# RN: PB, CE
# PB: CE, RN, PE
# PE: BA, PI, AL, CE, PB
# AL: SE, PE, BA
# SE: BA, AL
# BA: PI, PE, AL SE

# Podemos usar uma matriz de adjacência booleana para a implementação. O índice será de acordo com a ordem dada:
#              MA     PI     CE     RN     PB     PE     AL     SE     BA
*/
bool limites [9][9] = { {false,  true, false, false, false, false, false, false, false},   // MA
            { true, false,  true, false, false,  true, false, false,  true},   // PI
            {false,  true, false,  true,  true,  true, false, false, false},   // CE
            {false, false,  true, false,  true, false, false, false, false},   // RN
            {false, false,  true,  true, false,  true, false, false, false},   // PB
            {false,  true,  true, false,  true, false,  true, false,  true},   // PE
            {false, false, false, false, false,  true, false,  true,  true},   // AL
            {false, false, false, false, false, false,  true, false,  true},   // SE
            {false,  true, false, false, false,  true,  true,  true, false}};   // BA

// Domínio: 3 cores
string cores[3] = {"Vermelho", "Azul", "Verde"};
string estados[9] = {"MA", "PI", "CE", "RN", "PB", "PE", "AL", "SE", "BA"};

// Solução inicial aleatória
vector<string> solucao_inicial(9);

//Função para calcular o número de conflitos
tuple<int,set<int>> conta_conflitos(vector<string> &solucao){
    int conflitos=0;
    set<int> conflitantes;
    for(int estado1=0; estado1<9; estado1++)
        for(int estado2=estado1+1; estado2<9; estado2++)
            // Testa, para cada estado, se eles tem cores iguais e fazem fronteira ao mesmo tempo
            if(solucao[estado1] == solucao[estado2] && limites[estado1][estado2]){
                conflitos++;
                conflitantes.insert(estado1);
                conflitantes.insert(estado2);
			}
    return tuple<int, set<int>>(conflitos, conflitantes);
}

// Uma função para mostrar a solução final
void mostra_solucao(vector<string> solucao){
    cout << "Solução final:" << endl;
    for(int i=0;i<9;i++)
		cout << estados[i] << ": " << solucao[i] << endl;
    cout << "Número de conflitos: "  <<  get<0>(conta_conflitos(solucao)) << endl;
}

// A heurística de conflitos mínimos em si
vector<string> conflitos_minimos(vector<string> &solucao_inicial, int max_etapas){
    vector<string> atual = solucao_inicial;
    int conflitos;
	set<int> conflitantes;
    for(int iteracao; iteracao<max_etapas; iteracao++){
        tie(conflitos,conflitantes) = conta_conflitos(atual);
        if(conflitos == 0){
            cout << "O algoritmo encontrou a resposta!!!" << endl;
            break;
         }
		//Escolhe uma variável em conflito e muda seus valores, escolhendo o de conflitos mínimos
		auto it = conflitantes.begin();
		advance(it, rand()%conflitantes.size());
        int var_conflito = *it;
        vector<vector<string>>vizinhos;
        vector<int> conf_vizinhos;
        for(int i=0;i<3;i++){
            vector<string> copia = atual;
            copia[var_conflito]=cores[i];
            vizinhos.push_back(copia);
            conf_vizinhos.push_back(get<0>(conta_conflitos(copia)));
        }
        atual = vizinhos [ min_element(conf_vizinhos.begin(), conf_vizinhos.end()) - conf_vizinhos.begin() ];
    }
    return atual;
}

int main(){
	srand(time(nullptr));
	generate(solucao_inicial.begin(), solucao_inicial.end(),  [] () {return cores[rand()%3];});
	vector<string> solucao_final = conflitos_minimos(solucao_inicial, 1000);
	mostra_solucao(solucao_final);
	return 0;
}
